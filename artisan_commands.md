#### Artisan Commands

You all should be familiar with some of the commands right now. You can see a list of all the available commands with `php artisan list` and a brief description of what it does.

`php artisan tinker` allows you to interact with your Laravel application with the command lines, including Eloquent ORM, jobs, events and more.

---

#### Additional / Customize Commands

Commands are stored in `app/Console/Commands`.

In today lesson, we will create a command to output the products in the terminal. 

Execute in your terminal:

`php artisan make:command CheckProducts`

Before you can start using the command, you would have to register it at `app/Console/Kernel.php`. 

```php
protected $commands = [
    Commands\CheckProducts::class
];
```

Open up `app/Console/Commands/CheckProducts.php`.

Update `$signature` to:

`protected $signature = 'csh:check_product';`

Update `$description` to:

`protected $description = 'Check products';`

Now if you go to your terminal and execute `php artisan list`, you should be able to see your command is actually registered in the list.

---

Laravel has a few handy methods to output data in the command lines. 

Put the 3 lines below into your newly created command at `app/Console/Commands/CheckProducts.php` and put it in the `handle()` method. Then execute it `php artisan csh:check_product` at your terminal and see the output.

```php
 $this->info('Display this on the screen');
 $this->error('Something went wrong!');
 $this->line('Display this on the screen in plain text - no hightlighting');
```

You can also output it in table format easily. Try out this code at:

```php
$headers = ['Name', 'Price'];
$products = Product::get(['name', 'price'])->toArray();

$this->table($headers, $products);
```
 
You can have a progress bar if you have a lot of tasks or items to be processed. Comment out everything in your `handle()` method and replace with this, then give it a try.
 
 ```php
  $products = Product::get(['name', 'price'])->toArray();
  $bar = $this->output->createProgressBar(count($products));
  foreach ($products as $product) {
      sleep(1);
      $bar->advance();
  }
  $bar->finish();
 ```

You can also retrieve inputs from the users, optional or mandatory.
Update your `$signature` as below:

`protected $signature = 'csh:check_product {productId?}';`

We set the product ID input as optional by putting a `?` behind `productId`.

Replace the code in `handle()` method as below then give it a try.
 
```php
$productId = $this->argument('productId');
$products = new Product;
if ($productId) {
    $products = $products->where('id', $productId);
}
$products = $products->get(['name', 'price']);

$headers = ['Name', 'Price'];

$this->table($headers, $products->toArray());
```

Finally, you can have arguments, options, prompting for confirmation, calling commands from your controllers and much more. For a full list of what you can do, visit the [documentation](https://laravel.com/docs/5.4/artisan#registering-commands).