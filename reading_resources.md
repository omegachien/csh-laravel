### Reading Resources

[Laravel Documentation](https://laravel.com/docs/5.4/) - Official Laravel homepage

[Laravel News](https://laravel-news.com/) - All the latest news about Laravel

[Stackoverflow Laravel](http://stackoverflow.com/questions/tagged/laravel) - It's always best if you could help answering others' questions, if not you definitely can learn a thing or two from the questions.

[Laracasts](https://laracasts.com/) - Laravel, PHP, Vue, React online video tutorials.

[Hacker News](https://news.ycombinator.com/) - Science, programming, start up and entrepreneurship

Follow @taylorotwell, @themsaid, @freekmurze, @adamwathan, @stauffermatt, @laravelphp, @jeffrey_way on Twitter.



