<?php

namespace App\Console\Commands;

use App\Models\Product;
use Illuminate\Console\Command;

class CheckProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'csh:check_product {productId?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check products';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $productId = $this->argument('productId');
//        $products = new Product;
//        if ($productId) {
//            $products = $products->where('id', $productId);
//        }
//        $products = $products->get(['name', 'price']);
//
//        $headers = ['Name', 'Price'];
//
//        $this->table($headers, $products->toArray());
//        $this->info('Display this on the screen');
//        $this->error('Something went wrong!');
//        $this->line('Display this on the screen in plain text - no hightlighting');

//        $bar = $this->output->createProgressBar(count($products));
//        foreach ($products as $product) {
//            sleep(1);
//            $bar->advance();
//        }
//        $bar->finish();
    }
}
