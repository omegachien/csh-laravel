### Middleware ###

- filters HTTP requests entering your application
- think middleware as a series of "layers", which HTTP requests must pass through before arriving at your application
- located at `app/Http/Middleware`
- some example of middlewares included are:  
    - Authentication 
    - CSRF protection
    - Encrypting and decrypting cookies

---

Create middleware with command or of cause you can create it manually.

`php artisan make:middleware CheckAge`

---

### Global middleware

- run through every requests
- add the middleware class in `$middleware` array in `app/Http/Kernel.php`. 
 
 #### Middleware Group
 
 - You can also group middlewares together. For example:
 
 ```php
protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

        'api' => [
            'throttle:60,1',
            'bindings',
        ],
    ];

```

Out of the box, the web middleware group is automatically applied to your  routes/web.php file by the `app\Providers\RouteServiceProvider`.

You can apply middleware to your routes in the following ways:

```php
Route::get('backend/admin', function () {
    //
})->middleware('auth');

```

Assign multiple middleware

```php
Route::get('/', function () {
    //
})->middleware('first', 'second');
```
 
Or you can do it this way:
```php
Route::group(['middleware' => ['auth']], function () {
    //
});
```
---