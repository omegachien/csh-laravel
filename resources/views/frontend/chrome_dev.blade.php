<html>
<head>
  <title></title>
</head>

<body>
<p onclick="makeGreen()">Click me</p>

<script>
    function makeGreen() {
        var p = document.querySelector('p');
        p.style.color = '#BADA55';
        p.style.fontSize = '50px';
    }

    //regular
//    console.log('hello');

    //interpolated
//    console.log('Hello I am %s string', '💩');

    //styled
//    console.log('%c I am some great text', 'font-size:40px;');

    //warning
//    console.warn('Oh Nooooo');

    //error
//    console.error('oh my god error!');

    //info
//    console.info('Crocodiles eat 3-4 people per year');

    //Testing
//    console.assert(1 === 1, 'This is Wrong');

    //Clear
//    console.clear();

    //Viewing Dom element
//    var p = document.querySelector('p');
//    console.log(p);
//    console.dir(p);
//    console.clear();

    //Grouping Together
//    var dogs = [
//        {name: 'Roti Prata', age: 2, breed: 'roti'},
//        {name: 'Roti Telur', age: 3, breed: 'roti'}
//    ];
//    for (var i = 0; i < dogs.length; i++) {
//        console.group(dogs[i].name);
//        console.log(dogs[i].age);
//        console.log(dogs[i].breed);
//        console.groupEnd(dogs[i].name);
//    }

    //counting
//    console.count('JC');
//    console.count('JC');
//    console.count('JC');
//    console.count('JC');

    //timing
//    console.time('fetching data');
//    console.timeEnd('fetching data');

    //table
//    console.table(dogs);

</script>
</body>

</html>