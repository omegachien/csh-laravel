@extends('_layout.base_backend')

@section('css')
  <link href="http://hayageek.github.io/jQuery-Upload-File/4.0.10/uploadfile.css" rel="stylesheet">
  <style>
    .product-image {
      background-size: cover !important;
      background-position: center !important;
      width: 100%;
    }

    .delete-button {
      position: absolute;
      top: 0;
      right: 15px;
    }
  </style>
@endsection
@section('content')

  <h1 class="page-header">{{ $product->name }}</h1>
  <form id="form-delete" action="{{ url('backend/product/delete') }}" method="post">
    {{ csrf_field() }}
    <input name="id" class="hidden" value="{{$product->id}}">
  </form>
  <form action="{{ url('backend/product/update') }}" method="post">
    {{ csrf_field() }}
    <input name="id" class="hidden" value="{{$product->id}}">
    <div class="form-group">
      <label for="name">Name</label>
      <input type="text" name="name" class="form-control" id="name" placeholder="Name" value="{{$product->name}}">
    </div>
    <div class="form-group">
      <label for="price">Price</label>
      <input type="text" name="price" class="form-control" id="price" placeholder="0.00"
             value="{{$product->price}}">
    </div>

    <button type="submit" class="btn btn-primary">Update</button>
    <button id="btn-delete" type="button" class="btn btn-danger">Delete</button>
  </form>

  <hr>
  <h4>Upload Picture</h4>
  <div id="fileuploader">+</div>

  <hr>

  <div id="image-container">
    @foreach($product->images as $image)
      <div class="col-md-4" style="margin-bottom: 15px;">
        <img class="product-image" height="150" style="background: url('/{{$image->thumb_url}}');">
        <button id="{{$image->id}}" class="delete-button btn btn-danger">
          <span class="glyphicon glyphicon-trash"></span>
        </button>
      </div>
    @endforeach

    <div id="image-clone" class="col-md-4 hidden" style="margin-bottom: 15px;">
      <img class="product-image" height="150" style="background: url('/IMAGE_STUB');">
      <button id="IMAGE_ID_STUB" class="delete-button btn btn-danger">
        <span class="glyphicon glyphicon-trash"></span>
      </button>
    </div>

  </div>
@endsection

@section('js')
  <script src="http://hayageek.github.io/jQuery-Upload-File/4.0.10/jquery.uploadfile.min.js"></script>
  <script>
      $(document).ready(function () {
          $("#fileuploader").uploadFile({
              acceptFiles: "image/*",
              uploadStr: "+",
              url: "{{url('backend/product/image/upload')}}",
              formData: {
                  "_token": "{{csrf_token()}}",
                  "product_id": "{{$product->id}}",
              },
              fileName: "image",
              onSuccess: function (files, data, xhr) {
                  var url = data.thumb_url;
                  var productId = data.product_id;

                  var newImage = $("#image-clone").clone().removeAttr("id").removeClass("hidden");
                  newImage = newImage.prop("outerHTML");
                  newImage = newImage.replace("IMAGE_STUB", url);
                  newImage = newImage.replace("IMAGE_ID_STUB", productId);
                  $("#image-container").append(newImage);
              }
          });

          $("#btn-delete").click(function () {
              bootbox.confirm("Are you sure you want to delete this product?", function (result) {
                  if (result) {
                      $("#form-delete").submit();
                  }
              });
          });

          // Delete images
          $("#image-container").on("click", ".delete-button", function () {
              var toBeDeleteImage = $(this).parent();
              var formData = {
                  "_token": "{{csrf_token()}}",
                  "image_id": $(this).attr("id"),
              };

              $.ajax({
                  url: "/backend/product/image/delete",
                  type: "POST",
                  data: formData,
                  success: function (data, textStatus, jqXHR) {
                      toBeDeleteImage.slideUp();
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                      console.log(jqXHR.message);
                  }
              });
          });
      });
  </script>
@endsection