@extends('_layout.base_backend')

@section('content')
  <div class="page-header">
    <span class="h1">
      User
    </span>
    <div class="pull-right">
      <a class="btn btn-primary" href="{{ url('backend/user/create') }}">
        <i class="glyphicon glyphicon-plus"></i> &nbspCreate
      </a>
    </div>
  </div>

  <table id="user-table" class="table table-hover" style="width: 100%;">
    <thead>
    <tr>
      <th>Name</th>
      <th>Email</th>
      <th></th>
    </tr>
    </thead>

    <tbody>
    @foreach($users as $user)
      <tr id="{{$user->id}}" class="edit" style="cursor: pointer;">
        <td>{{$user->name}}</td>
        <td>{{$user->email}}</td>
        <td>
          <a href="{{ url('backend/user', $user->id . '/edit') }}">
            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
          </a>
        </td>
      </tr>
    @endforeach
    </tbody>
  </table>

@endsection

@section('js')
  <script>
      $(document).ready(function () {
//          $('#user-table').dataTable();
          $('table').dataTable({
              "aaSorting": [],
              "aoColumnDefs": [
                  {"bSortable": false, "aTargets": [2]}
              ]
          });
      });

      $(document).on("click", "tr.edit", function () {
          var userId = $(this).attr("id");
          window.location.href = "{{ url('backend/user') }}/" + userId  + "/edit";
      });
  </script>
@endsection