@extends('_layout.base_backend')

@section('title')
  Create User
@endsection

@section('content')

  <h1 class="page-header">Create New User</h1>

  <form action="{{ url('backend/user/create') }}" method="post">
    {{ csrf_field() }}
    <div class="form-group">
      <label for="name">Name</label>
      <input type="text" name="name" class="form-control" id="name" placeholder="Name" value="{{old('name')}}">
    </div>
    <div class="form-group">
      <label for="email">Email</label>
      <input type="text" name="email" class="form-control" id="email" placeholder="user@example.com"
             value="{{old('email')}}">
    </div>
    <div class="form-group">
      <label for="password">Password</label>
      <input type="password" name="password" class="form-control" id="password"
             value="">
    </div>
    <div class="form-group">
      <label for="email">Confirm Password</label>
      <input type="password" name="password_confirmation" class="form-control" id="password_confirmation"
             value="">
    </div>

    <button type="submit" class="btn btn-primary">Create</button>
  </form>

@endsection

@section('js')
  <script>
      $(document).ready(function () {

      });
  </script>
@endsection