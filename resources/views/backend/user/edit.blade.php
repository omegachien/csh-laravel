@extends('_layout.base_backend')

@section('content')

  <h1 class="page-header">{{ $user->name }}</h1>
  <form id="form-delete" action="{{ url('backend/user/delete') }}" method="post">
    {{ csrf_field() }}
    <input name="id" class="hidden" value="{{$user->id}}">
  </form>
  <form action="{{ url('backend/user/update') }}" method="post">
    {{ csrf_field() }}
    <input name="id" class="hidden" value="{{$user->id}}">
    <div class="form-group">
      <label for="name">Name</label>
      <input type="text" name="name" class="form-control" id="name" placeholder="Name" value="{{$user->name}}">
    </div>
    <div class="form-group">
      <label for="email">Email</label>
      <input type="text" name="email" class="form-control" id="email" placeholder="user@example.com"
             value="{{$user->email}}">
    </div>

    <button type="submit" class="btn btn-primary">Update</button>
    <button id="btn-delete" type="button" class="btn btn-danger">Delete</button>
  </form>

@endsection

@section('js')
  <script>
      $(document).ready(function () {
          $("#btn-delete").click(function () {
              bootbox.confirm("Are you sure you want to delete this user?", function (result) {
                  if(result) {
                      $("#form-delete").submit();
                  }
              });
          });
      });
  </script>
@endsection