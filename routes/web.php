<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/chrome-dev', 'HomeController@chromeDev');

Route::group(['prefix' => 'backend', 'middleware' => ['auth', 'role:1']], function() {
    Route::get('users', 'UserController@index');
    Route::get('user/{id}/edit', 'UserController@edit');
    Route::post('user/update', 'UserController@update');
    Route::post('user/delete', 'UserController@delete');
    Route::get('user/create', 'UserController@create');
    Route::post('user/create', 'UserController@store');
});

Route::group(['prefix' => 'backend', 'middleware' => 'auth'], function(){
    Route::get('products', 'ProductController@index');
    Route::get('a/products', 'ProductController@ajaxRetrieve');
    Route::get('product/create', 'ProductController@create');
    Route::post('product/create', 'ProductController@store');
    Route::get('product/{id}/edit', 'ProductController@edit');
    Route::post('product/update', 'ProductController@update');
    Route::post('product/delete', 'ProductController@delete');
    Route::post('product/image/upload', 'ProductController@upload');
    Route::post('product/image/delete', 'ProductController@imageDelete');
    Route::post('product/export', 'ProductController@export');
    Route::post('product/export-pdf', 'ProductController@exportPdf');

    Route::get('email', 'EmailController@index');
    Route::post('send-email', 'EmailController@send');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
